
{
    'name': 'Custom Debranding',
    'version': '10.0.1.0',
    'author': 'ehAPI',
    'category': 'Tools',
    'website': 'http://www.ehapi.com',
    'sequence': 0,
    'summary': 'Customizes and debrands your own ODOO',
    'description': """Customizes and debrands your own ODOO""",
    # 'images': ['static/description/banner.png'],
    'depends': ['web'],
    'data': [
        'views/app_odoo_customize_view.xml',
        'views/app_theme_config_settings_view.xml',
        'views/app_theme_config_settings_view.xml',
        # data
    ],
    'demo': [],
    'test': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [
        'static/src/xml/customize_user_menu.xml',

    ],
}

